pub fn run() {

}

mod json {
    use std::collections::HashMap;

    enum Numeric {
        Integer(usize),
        Float(f64)
    }

    enum JValue {
        JString(String),
        JNumeric(Numeric),
        JObject(HashMap<String,JValue>),
        JArray(Vec<JValue>),
        JBool(bool),
        JNull
    }
}
