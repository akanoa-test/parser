#[derive(Debug)]
#[derive(PartialEq)]
enum MatchType {
    CharList(Vec<char>),
    String(String),
    Int(u8),
}

impl MatchType {
    pub fn into_vec_char(self) -> Option<Vec<char>> {
        match self {
            MatchType::CharList(char_list) => Some(char_list),
            _ => None
        }
    }
}

#[derive(Debug)]
#[derive(PartialEq)]
struct MatchContainer {
    matches: MatchType,
    remaining: String,
}

impl MatchContainer {
    pub fn new(matches: MatchType, remaining: String) -> MatchContainer {
        MatchContainer {
            matches,
            remaining,
        }
    }
}

#[derive(Debug)]
#[derive(PartialEq)]
enum Result<T> {
    Success(T),
    Failure(String),
}


fn parse_a(input: &String) -> (bool, String) {
    let input = input.clone();

    if input.is_empty() {
        (false, "".to_string())
    } else if input.chars().next().unwrap() == 'A' {
        let remaining = input[1..].to_string();
        (true, remaining)
    } else {
        (false, input)
    }
}

fn parse_char_concrete(char_to_match: char, input: &String) -> Result<(Vec<char>, String)> {
    let input = input.clone();

    if input.is_empty() {
        Result::Failure("No more input".to_string())
    } else {
        let first = input.chars().next().unwrap();
        if first == char_to_match {
            let remaining = input[1..].to_string();
            Result::Success((vec![char_to_match], remaining))
        } else {
            let message = format!("Expecting '{}'. Got '{}'", char_to_match, first);
            Result::Failure(message)
        }
    }
}

fn parse_char_delayed(char_to_match: char) -> impl Fn(&String) -> Result<(Vec<char>, String)> {
    let inner_fn = move |input: &String| -> Result<(Vec<char>, String)>  {
        let input = input.clone();

        if input.is_empty() {
            Result::Failure("No more input".to_string())
        } else {
            let first = input.chars().next().unwrap();
            if first == char_to_match {
                let remaining = input[1..].to_string();
                Result::Success((vec![char_to_match], remaining))
            } else {
                let message = format!("Expecting '{}'. Got '{}'", char_to_match, first);
                Result::Failure(message)
            }
        }
    };

    inner_fn
}

fn run<F>(parser: F, input: &String) -> Result<MatchContainer>
    where F: Fn(&String) -> Result<MatchContainer>
{
    parser(input)
}

fn parse_char(char_to_match: char) -> impl Fn(&String) -> Result<MatchContainer> {
    let inner_fn = move |input: &String| -> Result<MatchContainer> {
        let input = input.clone();

        if input.is_empty() {
            Result::Failure("No more input".to_string())
        } else {
            let first = input.chars().next().unwrap();
            if first == char_to_match {
                let remaining = input[1..].to_string();
                let matches = MatchContainer::new(MatchType::CharList(vec![char_to_match]), remaining);
                Result::Success(matches)
            } else {
                let message = format!("Expecting '{}'. Got '{}'", char_to_match, first);
                Result::Failure(message)
            }
        }
    };

    inner_fn
}


fn and_then<F>(parser1: F, parser2: F) -> impl Fn(&String) -> Result<MatchContainer>
    where F: Fn(&String) -> Result<MatchContainer>
{
    let inner_fn = move |input: &String| -> Result<MatchContainer> {
        let result1 = run(&parser1, &input);
        match result1 {
            Result::Failure(err) => Result::Failure(err),
            Result::Success(matches1) => {
                let MatchContainer { matches: value1, remaining: remaining1 } = matches1;
                let result2 = run(&parser2, &remaining1);
                match result2 {
                    Result::Failure(err) => Result::Failure(err),
                    Result::Success(matches2) => {
                        let MatchContainer { matches: value2, remaining: remaining2 } = matches2;
                        let value1 = value1.into_vec_char().unwrap();
                        let value2 = value2.into_vec_char().unwrap();
                        let combined_value = MatchType::CharList(vec![value1[0], value2[0]]);
                        Result::Success(MatchContainer::new(combined_value, remaining2))
                    }
                }
            }
        }
    };

    inner_fn
}

fn or_else<F>(parser1: F, parser2: F) -> impl Fn(&String) -> Result<MatchContainer>
    where F: Fn(&String) -> Result<MatchContainer>
{
    let inner_fn = move |input: &String| -> Result<MatchContainer> {
        let result1 = run(&parser1, &input);

        match result1 {
            Result::Success(match1) => Result::Success(match1),
            Result::Failure(_) => {
                run(&parser2, &input)
            }
        }
    };

    inner_fn
}

fn map<F, P>(parser: P, f: F) -> impl Fn(&String) -> Result<MatchContainer>
    where F: Fn(MatchType) -> MatchType,
          P: Fn(&String) -> Result<MatchContainer>
{
    let inner_fn = move |input: &String| -> Result<MatchContainer> {
        let result = run(&parser, &input);

        match result {
            Result::Failure(err) => Result::Failure(err),
            Result::Success(matches) => {
                let MatchContainer { matches: value, remaining } = matches;
                let new_value = f(value);
                Result::Success(MatchContainer::new(new_value, remaining))
            }
        }
    };
    inner_fn
}

//fn reduce<F>( parsers: &[Box<Parser<MatchContainer>>], predicate: F) -> Box<Parser<MatchContainer>>
//where
//    F: Fn(Box<Parser<MatchContainer>>, Box<Parser<MatchContainer>>) -> Box<Parser<MatchContainer>>
//{
//
//    let mut parser = match parsers.get(0) {
//        Some(p) => p,
//        None => panic!("Empty parsers array")
//    };
//
//    let i = 1;
//
//    loop {
//
//        match parsers.get(i) {
//            None => break,
//            Some(ref parser2) => {
//                parser = predicate(parser, Box::new(***parser2))
//            }
//        }
//    }
//    parser
//}

#[cfg(test)]
mod tests {
    use crate::char::{parse_a, parse_char_concrete, parse_char, Result, parse_char_delayed, run, MatchContainer, MatchType, and_then, or_else, map};

    #[test]
    fn test_parse_A() {
        let input = "ABC".to_string();
        assert_eq!((true, "BC".to_string()), parse_a(&input));
        let input = "ZBC".to_string();
        assert_eq!((false, "ZBC".to_string()), parse_a(&input));
        let input = "".to_string();
        assert_eq!((false, "".to_string()), parse_a(&input));
    }

    #[test]
    fn test_parse_char_concrete() {
        let input = "ABC".to_string();
        let expected = Result::Success((vec!['A'], "BC".to_string()));
        assert_eq!(expected, parse_char_concrete('A', &input));
        let input = "ZBC".to_string();
        let expected = Result::Failure("Expecting 'A'. Got 'Z'".to_string());
        assert_eq!(expected, parse_char_concrete('A', &input));
        let input = "".to_string();
        let expected = Result::Failure("No more input".to_string());
        assert_eq!(expected, parse_char_concrete('A', &input));
    }

    #[test]
    fn test_parse_char_delayed() {
        let input = "ABC".to_string();
        let expected = Result::Success((vec!['A'], "BC".to_string()));
        assert_eq!(expected, parse_char_delayed('A')(&input));
        let input = "ZBC".to_string();
        let expected = Result::Failure("Expecting 'A'. Got 'Z'".to_string());
        assert_eq!(expected, parse_char_delayed('A')(&input));
        let input = "".to_string();
        let expected = Result::Failure("No more input".to_string());
        assert_eq!(expected, parse_char_delayed('A')(&input));
    }

    #[test]
    fn test_run() {
        let parse_A = parse_char('A');
        let input = "ABC".to_string();
        let expected = Result::Success(MatchContainer::new(MatchType::CharList(vec!['A']), "BC".to_string()));
        let result = run(&parse_A, &input);
        assert_eq!(expected, result);
        let input = "ZBC".to_string();
        let expected = Result::Failure("Expecting 'A'. Got 'Z'".to_string());
        let result = run(&parse_A, &input);
        assert_eq!(expected, result);
        let input = "".to_string();
        let expected = Result::Failure("No more input".to_string());
        let result = run(&parse_A, &input);
        assert_eq!(expected, result);
    }

    #[test]
    fn test_and_then() {
        let parser_A = parse_char('A');
        let parser_B = parse_char('B');
        let parser_AB = and_then(parser_A, parser_B);
        let input = "ABC".to_string();
        let expected = Result::Success(MatchContainer::new(MatchType::CharList(vec!['A', 'B']), "C".to_string()));
        let result = run(&parser_AB, &input);
        assert_eq!(expected, result);
        let input = "ZBC".to_string();
        let expected = Result::Failure("Expecting 'A'. Got 'Z'".to_string());
        let result = run(&parser_AB, &input);
        assert_eq!(expected, result);
        let input = "AZC".to_string();
        let expected = Result::Failure("Expecting 'B'. Got 'Z'".to_string());
        let result = run(&parser_AB, &input);
        assert_eq!(expected, result);
        let input = "".to_string();
        let expected = Result::Failure("No more input".to_string());
        let result = run(&parser_AB, &input);
        assert_eq!(expected, result);
    }

    #[test]
    fn test_or_else() {
        let parser_A = parse_char('A');
        let parser_B = parse_char('B');
        let parser_C = parse_char('B');
        let parser_AB = or_else(&parser_A, &parser_B);
        let parser_BA = or_else(&parser_B, &parser_A);
        let input = "ABC".to_string();
        let expected = Result::Success(MatchContainer::new(MatchType::CharList(vec!['A']), "BC".to_string()));
        let result = run(&parser_AB, &input);
        assert_eq!(expected, result);
        let input = "BBC".to_string();
        let expected = Result::Success(MatchContainer::new(MatchType::CharList(vec!['B']), "BC".to_string()));
        let result = run(&parser_AB, &input);
        assert_eq!(expected, result);
        let input = "ZBC".to_string();
        let expected = Result::Failure("Expecting 'B'. Got 'Z'".to_string());
        let result = run(&parser_AB, &input);
        assert_eq!(expected, result);
        let input = "".to_string();
        let expected = Result::Failure("No more input".to_string());
        let result = run(&parser_AB, &input);
        assert_eq!(expected, result);
    }

    #[test]
    fn test_map() {
        let input = "ABC".to_string();
        let expected = Result::Success(MatchContainer::new(MatchType::String("AB".to_string()), "C".to_string()));
        let parser_A = parse_char('A');
        let parser_B = parse_char('B');
        let parser_AB = and_then(&parser_A, &parser_B);

        let f = |value: MatchType| -> MatchType {
            match value {
                MatchType::CharList(char_list) => {
                    let concat = char_list.iter().collect::<String>();
                    MatchType::String(concat)
                },
                _ => value
            }
        };


        let mapped_parser_AB = map(&parser_AB, &f);
        let result = run(&mapped_parser_AB, &input);
        assert_eq!(expected, result);

        let input = "12C".to_string();
        let expected = Result::Success(MatchContainer::new(MatchType::Int(12), "C".to_string()));
        let parser_1 = parse_char('1');
        let parser_2 = parse_char('2');
        let parser_12 = and_then(&parser_1, &parser_2);

        let f = |value: MatchType| -> MatchType {
            match value {
                MatchType::CharList(char_list) => {
                    let concat = char_list.iter().collect::<String>().parse().unwrap();
                    MatchType::Int(concat)
                },
                _ => value
            }
        };


        let mapped_parser_12 = map(&parser_12, &f);
        let result = run(&mapped_parser_12, &input);
        assert_eq!(expected, result);
    }
}
